/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pornnongsaen.ahrthit.lab7;

/**
 *
 * @author MaeNzz
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


@WebServlet(name = "CallFac", urlPatterns = {"/CallFac"})
public class CallFac extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Call KKU Faculties</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>These are faculties at KKU</h1>");
            out.println("<ul>");
            
            JSONParser parser = new JSONParser();
            URL url = new URL("http://www.kku.ac.th/ikku/api/academics/services/getFaculty.php");
            URLConnection connURL = url.openConnection();
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(connURL.getInputStream()));
            StringBuilder builder = new StringBuilder();
            
            try {

                String read;
                String json = "";
                
                while((read = reader.readLine()) != null) {
                    builder.append(read);
                }
                reader.close();
                json = builder.toString();
                
                JSONObject obj = (JSONObject)parser.parse(json);
                JSONArray result = (JSONArray) obj.get("results");
                Iterator it = result.iterator();
                
                while(it.hasNext()) {
                    JSONObject inObj = (JSONObject) it.next();
                    String faculty = (String) inObj.get("name_en");
                    out.println("<li>" + faculty + "</li>");
                }
                
            } catch (Exception e) {
                System.err.println("Error occured!");
            }
            
            out.println("</ul>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

